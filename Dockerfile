FROM openjdk:16
VOLUME tmp
ADD target/songshop.jar songshop.jar
ADD src/main/resources/database/Chinook_Sqlite.sqlite src/main/resources/database/Chinook_Sqlite.sqlite
ENTRYPOINT ["java", "-jar", "songshop.jar"]