package com.songshop.songshop;

import com.songshop.songshop.dbhelper.CustomerRepositoryImpl;
import com.songshop.songshop.models.Customer;
import com.songshop.songshop.models.CustomerSpender;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;

// Equivalent of Main, run to run the application
@SpringBootApplication
public class SongshopApplication {
	public static void main(String[] args) {
		SpringApplication.run(SongshopApplication.class, args);
	}
}
