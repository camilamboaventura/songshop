package com.songshop.songshop.controllers;

import com.songshop.songshop.dbhelper.CustomerRepository;
import com.songshop.songshop.models.SearchResult;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;

@Controller
public class CostumerController {

    private final CustomerRepository customerRepository;

    public CostumerController(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    // Handles homepage model attributes
    @RequestMapping(value = "/", method = RequestMethod.GET)
        public String getRandomTrackNames(Model model){
            model.addAttribute("tracks", customerRepository.getRandomTracks());
        // Search warning is not displayed if false
            model.addAttribute("emptySearch", false);
            return "home";
        }


    @RequestMapping("/results")
    //Handles results page display
    public String getSearch(Model model, @RequestParam(required = false, name = "keyword") String keyword) {

        // Get the keyword from the @RequestParam and display list of search results
        model.addAttribute("keyword", keyword);
        ArrayList<SearchResult> list = customerRepository.getSearchResult(keyword);
        model.addAttribute("list", list);

        // Check if list is empty, do not go to results if true
        // and display warning instead
        if (list.isEmpty()) {
            model.addAttribute("emptySearch", true);
            model.addAttribute("tracks", customerRepository.getRandomTracks());
            return "home";
        } else {
            model.addAttribute("emptySearch", false);
            return "results";
        }
    }
}
