package com.songshop.songshop.controllers;

import com.songshop.songshop.dbhelper.CustomerRepository;
import com.songshop.songshop.models.*;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class DatabaseController {

    private final CustomerRepository customerRepository;

    public DatabaseController(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    // display all customers
    @RequestMapping(value="api/customers", method = RequestMethod.GET)
    public ArrayList<Customer> getAllCustomers() {
        return customerRepository.selectAllCustomers();
    }

    // display customers by id
    @RequestMapping(value = "api/customers/id={id}", method = RequestMethod.GET)
    public Customer getCustomerByHeaderId(@PathVariable(value="id") int id) {
        return customerRepository.selectSpecificCustomerById(id);
    }

    // display customer by name e.g.
    // http://localhost:8080/api/customers/name?first=Example&last=Test
    @RequestMapping(value = "api/customers/name", method = RequestMethod.GET)
    public Customer selectSpecificCustomerByName(@RequestParam String first,
                                          @RequestParam String last) {
        return customerRepository.selectSpecificCustomerByName(first, last);
    }

    // display customer by page e.g.
    //http://localhost:8080/api/customers/page=2
    @RequestMapping(value = "api/customers/page={page}", method = RequestMethod.GET)
    public ArrayList<Customer> selectCustomersPage(@PathVariable(value="page") int page) {
        return customerRepository.selectCustomersPage(page);
    }

    /*
     This adds a new customer.
     It takes the new customer from the body of the request.
    */
    @RequestMapping(value = "api/customers", method = RequestMethod.POST)
    public Boolean addCustomer(@RequestBody Customer customer){
        return customerRepository.addCustomer(customer);
    }


    // display customer by id
    @RequestMapping(value = "api/customers/{id}", method = RequestMethod.PUT)
    public Boolean updateCustomer(@PathVariable int id, @RequestBody Customer customer){
        return customerRepository.updateCustomer(customer);
    }

    // display customer amount in each country e.g.
    //http://localhost:8080/api/customers/countries
    @RequestMapping(value="api/customers/countries", method = RequestMethod.GET)
    public ArrayList<CustomerCountry> getCustomerAmountByCountry() {
        return customerRepository.getCustomerAmountByCountry();
    }

    // display customer list ordered by highest spenders at the top
    //http://localhost:8080/api/customers/topspender
    @RequestMapping(value="api/customers/topspender", method = RequestMethod.GET)
    public ArrayList<CustomerSpender> getSpenderByTotalInvoice() {
        return customerRepository.getSpenderByTotalInvoice();
    }

    // Display customer's favourite genre
    @RequestMapping(value = "api/customers/id={id}/favouritegenre", method = RequestMethod.GET)
    public CustomerGenre getFavouriteGenre(@PathVariable(value="id") int id) {
        return customerRepository.getFavouriteGenre(id);
    }


    /*
        Used to test @Controller requests
        ArrayList<RandomTrack> getRandomTracks();
        ArrayList<SearchResult> getSearchResult(String query);
    */

    // http://localhost:8080/api/random
    @RequestMapping(value="api/random", method = RequestMethod.GET)
    public ArrayList<RandomTrack> getRandomTrackNames() {
        return customerRepository.getRandomTracks();
    }

    // http://localhost:8080/api/search?query=
    @RequestMapping(value="api/search", method = RequestMethod.GET)
    public ArrayList<SearchResult> getSearch(@RequestParam String query) {
        return customerRepository.getSearchResult(query);
    }
}
