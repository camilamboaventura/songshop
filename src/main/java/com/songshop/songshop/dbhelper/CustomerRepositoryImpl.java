package com.songshop.songshop.dbhelper;

//import main.java.customers.Customer;

import com.songshop.songshop.logging.LogToConsole;
import com.songshop.songshop.models.*;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository {

    private final LogToConsole logger;
    // Setup
    // jdbc Java Database Connectivity (JDBC) API used to interact with SQLite
    // resource:Northwind_small.sqlite -> the resource a.k.a. the data
    private String URL = ConnectionHelper.CONNECTION_URL;

    public CustomerRepositoryImpl(LogToConsole logger) {
        this.logger = logger;
    }

    /*
     Need methods to serve the needs of the controller requests.
     Just mirror what your endpoints want.
    */


    public ArrayList<Customer> selectAllCustomers(){

        ArrayList<Customer> customers = new ArrayList<Customer>();

        try (Connection conn = DriverManager.getConnection(URL)) {
            System.out.println("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT CustomerId," +
                            "FirstName,LastName,Country,Phone,PostalCode,Email FROM customer");
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            // Process Results
            while (resultSet.next()) { //go through every single row of the resultSet table
                customers.add(
                        //store the current row as new customer Object in customers ArrayList<Customer>
                        new Customer(
                                resultSet.getInt("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("Country"),
                                resultSet.getString("PostalCode"),
                                resultSet.getString("Phone"),
                                resultSet.getString("Email")
                        ));
            }
        }
        catch (Exception ex){
            logger.log(ex.toString());
        }
        return customers;
    }

    public Customer selectSpecificCustomerById(int customerId){
        Customer customer = null;
        try (Connection conn = DriverManager.getConnection(URL)) {
            // Open Connection

            System.out.println("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT CustomerId,FirstName,LastName,Country,Phone,PostalCode,Email FROM CUSTOMER WHERE CustomerId = ?");
            preparedStatement.setString(1, String.valueOf(customerId)); // Corresponds to 1st '?' (must match type)
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            // Process Results
            while (resultSet.next()) {
                customer = new Customer(
                        resultSet.getInt("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getString("Phone"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Email")
                );
            }

        }
        catch (Exception ex){
            logger.log(ex.toString());
        }

        return customer;
    }

    public Customer selectSpecificCustomerByName(String firstName, String lastName){
        Customer customer = null;
        try(Connection conn = DriverManager.getConnection(URL)) {
            // Open Connection

            System.out.println("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                  conn.prepareStatement("SELECT CustomerId,FirstName,LastName,Country,Phone,PostalCode,Email FROM CUSTOMER WHERE FirstName = ? AND LastName = ?");
            preparedStatement.setString(1, firstName); // Corresponds to 1st '?' (must match type)
            preparedStatement.setString(2, lastName);
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            // Process Results
            while (resultSet.next()) {
                customer = new Customer(
                    resultSet.getInt("CustomerId"),
                    resultSet.getString("FirstName"),
                    resultSet.getString("LastName"),
                    resultSet.getString("Country"),
                    resultSet.getString("Phone"),
                    resultSet.getString("PostalCode"),
                    resultSet.getString("Email")
                );
            }

        }
        catch (Exception ex){
            logger.log(ex.toString());
        }

        return customer;
    }

    public ArrayList<Customer> selectCustomersPage(int page){
        ArrayList<Customer> customers = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(URL)) {
            // Open Connection

            System.out.println("Connection to SQLite has been established.");

            int offset = page*10;
            int limit = 10;
            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT * FROM\n" +
                            "(SELECT CustomerId,FirstName,LastName,\n" +
                            "Country,Phone,PostalCode,Email\n" +
                            "FROM CUSTOMER \n" +
                            "ORDER BY CustomerId ASC)\n" +
                            "LIMIT ? OFFSET ?");
            preparedStatement.setInt(1, limit); // Corresponds to 1st '?' (must match type)
            preparedStatement.setInt(2, offset);
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            // Process Results
            while (resultSet.next()) {
                Customer customer = new Customer(
                        resultSet.getInt("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getString("Phone"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Email")
                );
                customers.add(customer);
            }

        }
        catch (Exception ex){
            logger.log(ex.toString());
        }

        return customers;
    }

    public boolean addCustomer(Customer customer){

        boolean success = false;

        try (Connection conn = DriverManager.getConnection(URL)) {
            // Open Connection

            System.out.println("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("INSERT INTO CUSTOMER(CustomerId, FirstName, LastName, Country," +
                            "Phone, PostalCode, Email) VALUES(?, ?, ?, ?, ?, ?, ?)");

            preparedStatement.setInt(1, customer.getId()); // Corresponds to 1st '?' (must match type)
            preparedStatement.setString(2, customer.getFirstName());
            preparedStatement.setString(3, customer.getLastName());
            preparedStatement.setString(4, customer.getCountry());
            preparedStatement.setString(5, customer.getPhoneNumber());
            preparedStatement.setString(6, customer.getPostCode());
            preparedStatement.setString(7, customer.getEmail());

            // Execute Query
            int result = preparedStatement.executeUpdate();
            success = (result != 0);
            logger.log("Add customer successful");
        }
        catch (Exception ex){
            logger.log(ex.toString());
        }
        return success;
    }

    public boolean updateCustomer(Customer customer){

        try (Connection conn = DriverManager.getConnection(URL)) {
            // Open Connection

            System.out.println("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =

                    conn.prepareStatement("UPDATE customer SET CustomerId = ?, FirstName = ?, LastName = ?," +
                                    "Country = ?, Phone = ?, PostalCode = ?, Email = ? WHERE CustomerId = ?");

            preparedStatement.setInt(1, customer.getId()); // Corresponds to 1st '?' (must match type)
            preparedStatement.setString(2, customer.getFirstName());
            preparedStatement.setString(3, customer.getLastName());
            preparedStatement.setString(4, customer.getCountry());
            preparedStatement.setString(5, customer.getPhoneNumber());
            preparedStatement.setString(6, customer.getPostCode());
            preparedStatement.setString(7, customer.getEmail());
            preparedStatement.setInt(8, customer.getId());

            // Execute Statement
            preparedStatement.execute();
        }
        catch (Exception ex){
            logger.log(ex.toString());
            return false;
        }
        return true;
    }


    public ArrayList<CustomerCountry> getCustomerAmountByCountry(){

        ArrayList<CustomerCountry> customerCountryList = new ArrayList<CustomerCountry>();

        try (Connection conn = DriverManager.getConnection(URL)) {
            System.out.println("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT country, COUNT(country) AS NumberOfCountry " +
                            "FROM Customer " +
                            "GROUP by country " +
                            "ORDER by NumberOfCountry DESC");
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            // Process Results
            while (resultSet.next()) { //go through every single row of the resultSet table
                String country = resultSet.getString("Country");
                int number = resultSet.getInt("NumberOfCountry");
                CustomerCountry customerCountry = new CustomerCountry(country, number);

                customerCountryList.add(customerCountry);
            }

        }

        catch (Exception ex){
            logger.log(ex.toString());
        }

        return customerCountryList;
    }

    public ArrayList<CustomerSpender> getSpenderByTotalInvoice(){

        ArrayList<CustomerSpender> customerSpendersList = new ArrayList<CustomerSpender>();

        try (Connection conn = DriverManager.getConnection(URL)) {
            System.out.println("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT Total, Customer.CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email from Invoice\n" +
                            "INNER JOIN Customer ON Customer.CustomerId = Invoice.InvoiceId\n" +
                            "ORDER BY total DESC");
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            // Process Results
            while (resultSet.next()) { //go through every single row of the resultSet table
                double total = resultSet.getDouble("Total");

                Customer customer = new Customer(
                        resultSet.getInt("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getString("Phone"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Email")
                );

                CustomerSpender customerSpender = new CustomerSpender(total, customer);
                customerSpendersList.add(customerSpender);

            }

        }

        catch (Exception ex){
            logger.log(ex.toString());
        }

        return customerSpendersList;
    }


    public CustomerGenre getFavouriteGenre(int customerId){

        ArrayList<String> favourites = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(URL)) {
            System.out.println("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("WITH A AS (SELECT \n" +
                            "Invoice.CustomerId, \n" +
                            "Genre.Name,\n" +
                            "COUNT(Genre.Name) AS GenreCount\n" +
                            "FROM InvoiceLine\n" +
                            "JOIN Track on InvoiceLine.TrackId = Track.TrackId\n" +
                            "JOIN Invoice on Invoice.InvoiceId = InvoiceLine.InvoiceId\n" +
                            "JOIN Genre on Genre.GenreId = Track.GenreId\n" +
                            "WHERE CustomerId = ?\n" +
                            "GROUP BY Invoice.CustomerId, Genre.Name)\n" +
                            "SELECT * FROM A\n" +
                            "WHERE GenreCount = (SELECT Max(GenreCount) FROM A)");
            preparedStatement.setInt(1, customerId);

            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();


            // Process Results
            while (resultSet.next()) { //go through every single row of the resultSet table
                favourites.add(resultSet.getString("Name"));
            }

        }

        catch (Exception ex){
            logger.log(ex.toString());
        }

        return new CustomerGenre(favourites);
    }



    public ArrayList<RandomTrack> getRandomTracks(){

        ArrayList<RandomTrack> randomTracks = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(URL)) {
            System.out.println("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT \n" +
                            "Track.Name As Track, \n" +
                            "Artist.Name AS Artist, \n" +
                            "Genre.Name AS Genre\n" +
                            "FROM Track\n" +
                            "JOIN Genre ON Track.trackid = Genre.genreid\n" +
                            "JOIN Album ON Track.trackid = Album.albumid\n" +
                            "JOIN Artist ON Artist.ArtistId = Album.albumid\n" +
                            "ORDER BY RANDOM() \n" +
                            "LIMIT 5");

            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            // Process Results
            while (resultSet.next()) { //go through every single row of the resultSet table
                RandomTrack randomTrack = new RandomTrack(
                        resultSet.getString("Artist"),
                        resultSet.getString("Track"),
                        resultSet.getString("Genre")
                );

                randomTracks.add(randomTrack);
            }

        }

        catch (Exception ex){
            logger.log(ex.toString());
        }

        return randomTracks;
    }

    //SearchResult getSearchResult(String query);

    public ArrayList<SearchResult> getSearchResult(String query){

        ArrayList<SearchResult> results = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(URL)) {
            System.out.println("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT \n" +
                            "Track.Name As Track, \n" +
                            "Artist.Name AS Artist, \n" +
                            "Genre.Name AS Genre,\n" +
                            "Album.Title AS Album\n" +
                            "FROM Track\n" +
                            "JOIN Genre ON Track.trackid = Genre.genreid\n" +
                            "JOIN Album ON Track.trackid = Album.albumid\n" +
                            "JOIN Artist ON Artist.ArtistId = Album.albumid\n" +
                            "WHERE Track LIKE ?");

            preparedStatement.setString(1, "%" + query + "%");

            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            // Process Results
            while (resultSet.next()) { //go through every single row of the resultSet table
                SearchResult searchResult = new SearchResult(
                        resultSet.getString("Artist"),
                        resultSet.getString("Track"),
                        resultSet.getString("Genre"),
                        resultSet.getString("Album")
                );
                results.add(searchResult);
            }
        }

        catch (Exception ex){
            logger.log(ex.toString());
        }

        return results;
    }
}


