package com.songshop.songshop.models;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.HashMap;

// Class to store the customer and spending amount
// for highest spender method

public class CustomerSpender {

    private double amountSpent;
    private Customer customer;

    public CustomerSpender(double amountSpent, Customer customer) {
        this.amountSpent = amountSpent;
        this.customer = customer;
    }

    public double getAmountSpent() {
        return amountSpent;
    }

    public void setAmountSpent(double amountSpent) {
        this.amountSpent = amountSpent;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}


