package com.songshop.songshop.models;

// Class to store search results for the results page

public class SearchResult extends RandomTrack {
    private String album;

    public SearchResult(String artist, String track, String genre, String album) {
        super(artist, track, genre);
        this.album = album;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    @Override
    public String toString() {
        return "SearchResult{" +
                "album='" + album + '\'' +
                '}';
    }
}
